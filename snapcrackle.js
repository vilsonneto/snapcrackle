const snapCrackle = (maxVallue) => {

    let frase = "" ;

    for ( let i = 1; i <= maxVallue; i++) {

        if ( i % 2 != 0 ) {
            
            if ( i % 5 == 0 ) {
    
                frase += "SnapCrackle, ";
    
            }
            else {

                frase +=  "Snap, ";
            }

        }

        else if ( i % 5 == 0 ) {

            frase += "Crackle, ";

        }
        
        else {

            frase += `${ i }, `;

        }
        
    }

    return  console.log(frase)

}

snapCrackle(12)